﻿using Common.Models;

namespace DataAccess.Xml;

public class XmlContext
{
    public List<Player> Players { get; } = new();
    public List<Game> Games { get; } = new();
}