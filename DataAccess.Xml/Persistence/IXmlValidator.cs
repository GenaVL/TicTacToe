﻿using System.Xml.Linq;

namespace DataAccess.Xml.Persistence;

public interface IXmlValidator
{
    void Validate(XDocument doc);
}