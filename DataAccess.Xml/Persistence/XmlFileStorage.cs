﻿using System.Xml.Linq;

namespace DataAccess.Xml.Persistence;

public class XmlFileStorage : IXmlStorage
{
    public XmlFileStorage(string xmlPath)
    {
        if (string.IsNullOrEmpty(xmlPath)) throw new ArgumentNullException(nameof(xmlPath));
        FilePath = xmlPath;
    }

    public string FilePath { get; }

    public XDocument Load()
    {
        using var stream = File.Open(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read);

        var doc = XDocument.Load(stream, LoadOptions.None);
        return doc;
    }

    public void Save(XDocument doc)
    {
        using var stream = File.Open(FilePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);

        doc.Save(stream, SaveOptions.None);
    }
}