﻿using System.Xml.Linq;
using Common.Models;
using Common.Models.Enums;

namespace DataAccess.Xml.Persistence;

public class XmlSerializer : IXmlSerializer
{
    public XmlContext Deserialize(XDocument xDocument)
    {
        var context = new XmlContext();
        var root = xDocument.Root;

        DeserializePlayerSection(root, context);
        DeserializeGameSection(root, context);


        return context;
    }

    public XDocument Serialize(XmlContext context)
    {
        var root = new XElement(Context);
        var doc = new XDocument(root);

        SerializePlayerSection(root, context);
        SerializeGameSection(root, context);

        return doc;
    }

    #region Node Constants

    public const string Context = "Context";

    public const string PlayerSection = "Players";
    public const string Player = "Player";

    public const string GameSection = "Games";
    public const string Game = "Game";
    public const string MoveSection = "Moves";
    public const string Move = "Move";

    public const string Id = "Id";
    public const string CrossId = "CrossId";
    public const string ZeroId = "ZeroId";
    public const string State = "State";
    public const string Name = "Name";


    public const string Number = "Number";
    public const string XAttr = "X";
    public const string YAttr = "Y";

    public const string DateTimeFormat = "yyyyMMddHHmmss";
    public const string GuidFormat = "n";

    #endregion

    #region Serialize

    private static void SerializePlayerSection(XElement root, XmlContext context)
    {
        var xmlPlayerSection = new XElement(PlayerSection);
        root.Add(xmlPlayerSection);
        foreach (var player in context.Players)
        {
            var xmlPlayerElement = new XElement(Player);
            xmlPlayerElement.Add(new XAttribute(Id, player.Id.ToString(GuidFormat)));
            xmlPlayerElement.Add(new XAttribute(Name, player.Name));
            xmlPlayerSection.Add(xmlPlayerElement);
        }
    }

    private static void SerializeGameSection(XElement root, XmlContext context)
    {
        var xmlGameSection = new XElement(GameSection);
        root.Add(xmlGameSection);
        foreach (var game in context.Games)
        {
            var xmlGameElement = new XElement(Game);
            xmlGameElement.Add(new XAttribute(Id, game.Id.ToString(GuidFormat)));
            xmlGameElement.Add(new XAttribute(CrossId, game.CrossId.ToString(GuidFormat)));
            xmlGameElement.Add(new XAttribute(ZeroId, game.ZeroId.ToString(GuidFormat)));
            xmlGameElement.Add(new XAttribute(State, (int)game.State));

            var xmlMovesSection = new XElement(MoveSection);
            xmlGameElement.Add(xmlMovesSection);

            xmlGameSection.Add(xmlGameElement);

            foreach (var move in game.Moves)
            {
                var xmlMoveElement = new XElement(Move);
                xmlMoveElement.Add(new XAttribute(Id, move.Id.ToString(GuidFormat)));
                xmlMoveElement.Add(new XAttribute(Number, move.Number));
                xmlMoveElement.Add(new XAttribute(XAttr, move.X));
                xmlMoveElement.Add(new XAttribute(YAttr, move.Y));

                xmlMovesSection.Add(xmlMoveElement);
            }
        }
    }

    #endregion

    #region Deserialize

    private static void DeserializePlayerSection(XElement root, XmlContext context)
    {
        var xmlPlayerSection = root.Element(PlayerSection);
        var xmlPlayerElements = xmlPlayerSection!.Elements().ToList();
        foreach (var xmlPlayerElement in xmlPlayerElements)
        {
            var player = new Player
            {
                Id = Guid.Parse(xmlPlayerElement.Attribute(Id)!.Value),
                Name = xmlPlayerElement.Attribute(Name)!.Value
            };
            context.Players.Add(player);
        }
    }


    private static void DeserializeGameSection(XElement root, XmlContext context)
    {
        var xmlGameSection = root.Element(GameSection);
        var xmlGameElements = xmlGameSection!.Elements().ToList();
        foreach (var xmlGameElement in xmlGameElements)
        {
            var game = new Game
            {
                Id = Guid.Parse(xmlGameElement.Attribute(Id)!.Value),
                CrossId = Guid.Parse(xmlGameElement.Attribute(CrossId)!.Value),
                ZeroId = Guid.Parse(xmlGameElement.Attribute(ZeroId)!.Value),
                State = (GameState)int.Parse(xmlGameElement.Attribute(State)!.Value)
            };
            game.Cross = context.Players.First(p => p.Id == game.CrossId);
            game.Zero = context.Players.First(p => p.Id == game.ZeroId);

            var xmlMoveSection = xmlGameElement.Element(MoveSection)!;
            var xmlMoveElements = xmlMoveSection!.Elements().ToList();
            foreach (var xmlMoveElement in xmlMoveElements)
            {
                var move = new Move
                {
                    Id = Guid.Parse(xmlMoveElement.Attribute(Id)!.Value),
                    Number = int.Parse(xmlMoveElement.Attribute(Number)!.Value),
                    X = int.Parse(xmlMoveElement.Attribute(XAttr)!.Value),
                    Y = int.Parse(xmlMoveElement.Attribute(YAttr)!.Value)
                };
                game.Moves.Add(move);
            }

            context.Games.Add(game);
        }
    }

    #endregion
}