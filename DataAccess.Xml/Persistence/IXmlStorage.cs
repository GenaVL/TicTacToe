﻿using System.Xml.Linq;

namespace DataAccess.Xml.Persistence;

public interface IXmlStorage
{
    XDocument Load();
    void Save(XDocument doc);
}