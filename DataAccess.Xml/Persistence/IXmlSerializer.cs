﻿using System.Xml.Linq;

namespace DataAccess.Xml.Persistence;

public interface IXmlSerializer
{
    XDocument Serialize(XmlContext context);
    XmlContext Deserialize(XDocument document);
}