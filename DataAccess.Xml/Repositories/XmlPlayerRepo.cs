﻿using Common.DataAccess;
using Common.Models;

namespace DataAccess.Xml.Repositories;

public class XmlPlayerRepo : XmlBaseRepo, IPlayerRepo
{
    public Player GetById(Guid id)
    {
        return Context.Players.FirstOrDefault(p => p.Id == id);
    }

    public List<Player> GetList()
    {
        return Context.Players;
    }
}