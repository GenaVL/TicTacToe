﻿using DataAccess.Xml.Persistence;
using Infrastructure;
using Infrastructure.Peaa;

namespace DataAccess.Xml;

public class XmlUnitOfWorkFactory : IUnitOfWorkFactory
{
    private readonly IXmlSerializer _serializer;
    private readonly ServiceFactory _serviceFactory;
    private readonly IXmlStorage _storage;
    private readonly IXmlValidator _validator;

    public XmlUnitOfWorkFactory(ServiceFactory serviceFactory, IXmlSerializer serializer, IXmlStorage storage,
        IXmlValidator validator)
    {
        _serviceFactory = serviceFactory;
        _serializer = serializer;
        _storage = storage;
        _validator = validator;
    }

    public IUnitOfWork Create()
    {
        return new XmlUnitOfWork(_serviceFactory, _serializer, _storage, _validator);
    }
}