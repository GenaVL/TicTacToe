﻿using DataAccess.Xml.Persistence;
using DataAccess.Xml.Repositories;
using Infrastructure;
using Infrastructure.Peaa;

namespace DataAccess.Xml;

public class XmlUnitOfWork : IUnitOfWork
{
    private readonly IXmlSerializer _serializer;
    private readonly ServiceFactory _serviceFactory;
    private readonly IXmlStorage _storage;
    private readonly IXmlValidator _validator;

    private XmlContext _context;

    public XmlUnitOfWork(
        ServiceFactory serviceFactory,
        IXmlSerializer serializer,
        IXmlStorage storage,
        IXmlValidator validator)
    {
        _serviceFactory = serviceFactory;
        _serializer = serializer;
        _storage = storage;
        _validator = validator;
    }

    public T GetRepository<T>()
    {
        var repository = _serviceFactory.GetInstance<T>();
        if (!(repository is XmlBaseRepo repo)) throw new InvalidCastException($"{typeof(T)}");
        repo.Context = GetContext();
        return repository;
    }

    public void RejectChanges()
    {
        _context = null;
        GetContext();
    }

    public void SaveChanges()
    {
        SaveContext();
    }

    public void Dispose()
    {
    }

    private XmlContext GetContext()
    {
        if (_context == null)
        {
            var doc = _storage.Load();

            _validator.Validate(doc);

            _context = _serializer.Deserialize(doc);
        }

        return _context;
    }

    public void SaveContext()
    {
        var doc = _serializer.Serialize(_context);
        _storage.Save(doc);
    }
}