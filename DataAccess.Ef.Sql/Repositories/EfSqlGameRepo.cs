﻿using Common.DataAccess;
using Common.Models;

namespace DataAccess.Ef.Sql.Repositories;

public class EfSqlGameRepo : EfSqlBaseRepo, IGameRepo
{
    public Game GetById(Guid id)
    {
        return Context.Games.FirstOrDefault(g => g.Id == id);
    }

    public void AddGame(Game game)
    {
        Context.Games.Add(game);
    }

    public void UpdateGame(Game game)
    {
        var gameForDelete = GetById(game.Id);
        Context.Games.Remove(gameForDelete);
        Context.Games.Add(game);
    }
}