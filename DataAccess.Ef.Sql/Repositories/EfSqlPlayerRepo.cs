﻿using Common.DataAccess;
using Common.Models;

namespace DataAccess.Ef.Sql.Repositories;

public class EfSqlPlayerRepo : EfSqlBaseRepo, IPlayerRepo
{
    public Player GetById(Guid id)
    {
        return Context.Players.FirstOrDefault(p => p.Id == id);
    }

    public List<Player> GetList()
    {
        return Context.Players.ToList();
    }
}