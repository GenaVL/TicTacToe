﻿using Common.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Ef.Sql;

public class EfSqlContext : DbContext
{
    public DbSet<Game> Games { get; set; }
    public DbSet<Move> Moves { get; set; }
    public DbSet<Player> Players { get; set; }
}