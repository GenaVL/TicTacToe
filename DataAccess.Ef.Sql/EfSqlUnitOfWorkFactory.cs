﻿using Infrastructure;
using Infrastructure.Peaa;

namespace DataAccess.Ef.Sql;

public class EfSqlUnitOfWorkFactory : IUnitOfWorkFactory
{
    private readonly ServiceFactory _serviceFactory;

    public EfSqlUnitOfWorkFactory(ServiceFactory serviceFactory)
    {
        _serviceFactory = serviceFactory;
    }

    public IUnitOfWork Create()
    {
        return new EfSqlUnitOfWork(_serviceFactory);
    }
}