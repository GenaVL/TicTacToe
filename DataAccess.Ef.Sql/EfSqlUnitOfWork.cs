﻿using DataAccess.Ef.Sql.Repositories;
using Infrastructure;
using Infrastructure.Peaa;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Ef.Sql;

public class EfSqlUnitOfWork : IUnitOfWork
{
    private readonly ServiceFactory _serviceFactory;

    private EfSqlContext _context;

    public EfSqlUnitOfWork(ServiceFactory serviceFactory)
    {
        _serviceFactory = serviceFactory;
    }

    public T GetRepository<T>()
    {
        var repository = _serviceFactory.GetInstance<T>();
        if (!(repository is EfSqlBaseRepo repo)) throw new InvalidCastException($"{typeof(T)}");
        repo.Context = GetContext();
        return repository;
    }

    public void RejectChanges()
    {
        if (_context != null)
            foreach (var entry in _context.ChangeTracker.Entries())
                switch (entry.State)
                {
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                }
    }

    public void SaveChanges()
    {
        SaveContext();
    }

    public void Dispose()
    {
        if (_context != null) _context.Dispose();
    }

    private EfSqlContext GetContext()
    {
        if (_context == null) _context = new EfSqlContext();

        return _context;
    }

    public void SaveContext()
    {
        if (_context != null) _context.SaveChanges();
    }
}