﻿using System.Net.Http.Json;
using Common.Business;
using Common.Models;

namespace WebApi.Proxy;

public class PlayerProxyService : IPlayerService
{
    public List<Player> GetList()
    {
        using var client = new HttpClient();

        var url = "http://localhost:5298/api/player";
        var playersTask = client.GetFromJsonAsync<List<Player>>(url);

        var players = playersTask.Result;

        return players;
    }
}