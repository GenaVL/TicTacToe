﻿using System.Net.Http.Json;
using Common.Business;
using Common.Models;
using Infrastructure;

namespace WebApi.Proxy;

public class GameProxyService : IGameService
{
    public Game CreateGame(Guid crossId, Guid zeroId)
    {
        using var client = new HttpClient();

        var url =
            $"http://localhost:5298/api/game/create/{crossId.ToString(Formats.GuidFormat)}/{zeroId.ToString(Formats.GuidFormat)}";

        var responseTask = client.PostAsync(url, null);
        var response = responseTask.Result;

        if (response.IsSuccessStatusCode)
        {
            var gameTask = response.Content.ReadFromJsonAsync<Game>();
            var game = gameTask.Result;
            return game;
        }

        return new Game();
    }

    public Game MakeNextMove(Guid gameId, int x, int y)
    {
        using var client = new HttpClient();

        var url = $"http://localhost:5298/api/game/nextmove/{gameId.ToString(Formats.GuidFormat)}-{x}-{y}";

        var responseTask = client.PostAsync(url, null);
        var response = responseTask.Result;

        if (response.IsSuccessStatusCode)
        {
            var gameTask = response.Content.ReadFromJsonAsync<Game>();
            var game = gameTask.Result;
            return game;
        }

        return new Game();
    }

    public Game GetGame(Guid gameId)
    {
        using var client = new HttpClient();

        var url = $"http://localhost:5298/api/game/{gameId.ToString(Formats.GuidFormat)}";
        var fromJsonAsync = client.GetFromJsonAsync<Game>(url);

        var game = fromJsonAsync.Result;

        return game;
    }
}