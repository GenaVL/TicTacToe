﻿using Common.Business;
using Common.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Business.Controllers;

[Route("api/[controller]")]
[ApiController]
public class PlayerController : ControllerBase
{
    private readonly IPlayerService _playerService;

    public PlayerController(IPlayerService playerService)
    {
        _playerService = playerService;
    }

    [HttpGet]
    public List<Player> Get()
    {
        return _playerService.GetList();
    }
}