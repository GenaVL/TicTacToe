﻿using Common.Business;
using Common.Models;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Business.Controllers;

[Route("api/[controller]")]
[ApiController]
public class GameController : ControllerBase
{
    private readonly IGameService _gameService;

    public GameController(IGameService gameService)
    {
        _gameService = gameService;
    }

    [HttpGet("{id}")]
    public Game Get(Guid id)
    {
        return _gameService.GetGame(id);
    }

    [HttpPost("create/{crossId}/{zeroId}")]
    public Game Post(Guid crossId, Guid zeroId)
    {
        return _gameService.CreateGame(crossId, zeroId);
    }

    [HttpPost("nextmove/{id}-{x}-{y}")]
    public Game Post(Guid id, int x, int y)
    {
        return _gameService.MakeNextMove(id, x, y);
    }
}