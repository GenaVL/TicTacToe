using Business;
using Common.Business;
using Common.DataAccess;
using DataAccess.Xml;
using DataAccess.Xml.Persistence;
using DataAccess.Xml.Repositories;
using Infrastructure;
using Infrastructure.Peaa;

namespace WebApp.Business;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.

        builder.Services.AddControllers();

        ConfigureServices(builder.Services);

        var app = builder.Build();

        // Configure the HTTP request pipeline.

        app.UseAuthorization();


        app.MapControllers();

        app.Run();
    }


    private static void ConfigureServices(IServiceCollection services)
    {
        services.AddScoped<ServiceFactory>(sp => sp.GetRequiredService);

        services.AddScoped<IGameService, GameService>();
        services.AddScoped<IPlayerService, PlayerService>();

        services.AddScoped<IUnitOfWorkFactory, XmlUnitOfWorkFactory>();

        services.AddScoped<IXmlSerializer, XmlSerializer>();
        services.AddScoped<IXmlStorage>(sp =>
        {
            var configuration = sp.GetRequiredService<IConfiguration>();
            var filePath = configuration.GetValue<string>("FilePath");
            return new XmlFileStorage(filePath);
        });
        services.AddScoped<IXmlValidator, XmlValidator>();

        services.AddScoped<IPlayerRepo, XmlPlayerRepo>();
        services.AddScoped<IGameRepo, XmlGameRepo>();
    }
}