﻿namespace Infrastructure;

public static class Formats
{
    public const string DateTimeFormat = "yyyyMMddHHmmss";
    public const string GuidFormat = "n";
}