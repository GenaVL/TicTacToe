﻿namespace Infrastructure.Peaa;

public interface IUnitOfWork : IRepositoryFactory, IStoreManager, IDisposable
{
}