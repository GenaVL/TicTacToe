﻿namespace Infrastructure.Peaa;

public interface IUnitOfWorkFactory
{
    IUnitOfWork Create();
}