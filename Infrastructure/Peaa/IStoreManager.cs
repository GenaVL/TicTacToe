﻿namespace Infrastructure.Peaa;

public interface IStoreManager
{
    void RejectChanges();
    void SaveChanges();
}