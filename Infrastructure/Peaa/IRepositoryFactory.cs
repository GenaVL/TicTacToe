﻿namespace Infrastructure.Peaa;

public interface IRepositoryFactory
{
    T GetRepository<T>();
}