﻿using Business;
using Common.Business;
using Common.DataAccess;
using DataAccess.Ef.Sql;
using DataAccess.Ef.Sql.Repositories;
using DataAccess.Json;
using DataAccess.Json.Persistence;
using DataAccess.Json.Repositories;
using DataAccess.Xml;
using DataAccess.Xml.Persistence;
using DataAccess.Xml.Repositories;
using Infrastructure;
using Infrastructure.Peaa;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApi.Proxy;

namespace UI.BlazorApp.Di
{
    public class Configuration
    {
        public static void ConfigureServices(IServiceCollection services, string diValue)
        {
            services.AddScoped<ServiceFactory>(sp => sp.GetRequiredService);

            if (diValue == "Proxy")
            {
                services.AddScoped<IGameService, GameProxyService>();
                services.AddScoped<IPlayerService, PlayerProxyService>();
                return;
            }
            services.AddScoped<IGameService, GameService>();
            services.AddScoped<IPlayerService, PlayerService>();

            if (diValue == "Xml")
            {
                services.AddScoped<IUnitOfWorkFactory, XmlUnitOfWorkFactory>();

                services.AddScoped<IXmlSerializer, XmlSerializer>();
                services.AddScoped<IXmlStorage>(sp =>
                {
                    var configuration = sp.GetRequiredService<IConfiguration>();
                    var filePath = configuration.GetValue<string>("XmlFilePath");
                    return new XmlFileStorage(filePath);
                });
                services.AddScoped<IXmlValidator, XmlValidator>();

                services.AddScoped<IPlayerRepo, XmlPlayerRepo>();
                services.AddScoped<IGameRepo, XmlGameRepo>();
                return;
            }
            if (diValue == "Json")
            {
                services.AddScoped<IUnitOfWorkFactory, JsonUnitOfWorkFactory>();

                services.AddScoped<IJsonSerializer, JsonSerializer>();
                services.AddScoped<IJsonStorage>(sp =>
                {
                    var configuration = sp.GetRequiredService<IConfiguration>();
                    var filePath = configuration.GetValue<string>("JsonFilePath");
                    return new JsonFileStorage(filePath);
                });

                services.AddScoped<IPlayerRepo, JsonPlayerRepo>();
                services.AddScoped<IGameRepo, JsonGameRepo>();
                return;
            }
            if (diValue == "EfSql")
            {
                services.AddScoped<IUnitOfWorkFactory, EfSqlUnitOfWorkFactory>();

                services.AddScoped<IPlayerRepo, EfSqlPlayerRepo>();
                services.AddScoped<IGameRepo, EfSqlGameRepo>();
                return;
            }

            throw new NotSupportedException("Configuration");

        }

    }
}
