﻿namespace UI.WinForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            _settingsPanel = new Panel();
            _clearButton = new Button();
            _zeroPlayerListBox = new ListBox();
            _crossPlayerListBox = new ListBox();
            _gameStateLabel = new Label();
            _startButton = new Button();
            _gamePanel = new DrawPanel();
            _settingsPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _settingsPanel
            // 
            _settingsPanel.Controls.Add(_clearButton);
            _settingsPanel.Controls.Add(_zeroPlayerListBox);
            _settingsPanel.Controls.Add(_crossPlayerListBox);
            _settingsPanel.Controls.Add(_gameStateLabel);
            _settingsPanel.Controls.Add(_startButton);
            _settingsPanel.Dock = DockStyle.Right;
            _settingsPanel.Location = new Point(641, 0);
            _settingsPanel.Margin = new Padding(3, 4, 3, 4);
            _settingsPanel.Name = "_settingsPanel";
            _settingsPanel.Padding = new Padding(23, 27, 23, 27);
            _settingsPanel.Size = new Size(369, 748);
            _settingsPanel.TabIndex = 0;
            // 
            // _clearButton
            // 
            _clearButton.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 204);
            _clearButton.Location = new Point(192, 668);
            _clearButton.Margin = new Padding(0);
            _clearButton.Name = "_clearButton";
            _clearButton.Size = new Size(150, 53);
            _clearButton.TabIndex = 5;
            _clearButton.Text = "Clear";
            _clearButton.UseVisualStyleBackColor = true;
            _clearButton.Click += _clearButton_Click;
            // 
            // _zeroPlayerListBox
            // 
            _zeroPlayerListBox.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 204);
            _zeroPlayerListBox.FormattingEnabled = true;
            _zeroPlayerListBox.ItemHeight = 32;
            _zeroPlayerListBox.Items.AddRange(new object[] { "AAAAAAAAAAA", "AAAAAAAAAAA", "AAAAAAAAAAA", "AAAAAAAAAAA", "AAAAAAAAAAA", "AAAAAAAAAAA", "AAAAAAAAAAA", "AAAAAAAAAAA" });
            _zeroPlayerListBox.Location = new Point(30, 293);
            _zeroPlayerListBox.Margin = new Padding(3, 4, 3, 4);
            _zeroPlayerListBox.Name = "_zeroPlayerListBox";
            _zeroPlayerListBox.Size = new Size(316, 228);
            _zeroPlayerListBox.TabIndex = 4;
            _zeroPlayerListBox.SelectedIndexChanged += _zeroPlayerListBox_SelectedIndexChanged;
            // 
            // _crossPlayerListBox
            // 
            _crossPlayerListBox.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 204);
            _crossPlayerListBox.FormattingEnabled = true;
            _crossPlayerListBox.ItemHeight = 32;
            _crossPlayerListBox.Items.AddRange(new object[] { "AAAAAAAAAAA", "AAAAAAAAAAA", "AAAAAAAAAAA", "AAAAAAAAAAA", "AAAAAAAAAAA", "AAAAAAAAAAA", "AAAAAAAAAAA", "AAAAAAAAAAA" });
            _crossPlayerListBox.Location = new Point(26, 31);
            _crossPlayerListBox.Margin = new Padding(3, 4, 3, 4);
            _crossPlayerListBox.Name = "_crossPlayerListBox";
            _crossPlayerListBox.Size = new Size(316, 228);
            _crossPlayerListBox.TabIndex = 3;
            _crossPlayerListBox.SelectedIndexChanged += _crossPlayerListBox_SelectedIndexChanged;
            // 
            // _gameStateLabel
            // 
            _gameStateLabel.Font = new Font("Segoe UI", 14.25F, FontStyle.Bold, GraphicsUnit.Point, 204);
            _gameStateLabel.ForeColor = Color.FromArgb(0, 192, 0);
            _gameStateLabel.Location = new Point(23, 588);
            _gameStateLabel.Margin = new Padding(0);
            _gameStateLabel.Name = "_gameStateLabel";
            _gameStateLabel.Size = new Size(323, 53);
            _gameStateLabel.TabIndex = 2;
            _gameStateLabel.Text = "label1";
            _gameStateLabel.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // _startButton
            // 
            _startButton.Font = new Font("Segoe UI", 14.25F, FontStyle.Regular, GraphicsUnit.Point, 204);
            _startButton.Location = new Point(23, 668);
            _startButton.Margin = new Padding(0);
            _startButton.Name = "_startButton";
            _startButton.Size = new Size(150, 53);
            _startButton.TabIndex = 1;
            _startButton.Text = "Start";
            _startButton.UseVisualStyleBackColor = true;
            _startButton.Click += _startButton_Click;
            // 
            // _gamePanel
            // 
            _gamePanel.BackColor = SystemColors.AppWorkspace;
            _gamePanel.Dock = DockStyle.Fill;
            _gamePanel.Location = new Point(0, 0);
            _gamePanel.Margin = new Padding(3, 4, 3, 4);
            _gamePanel.Name = "_gamePanel";
            _gamePanel.Size = new Size(641, 748);
            _gamePanel.TabIndex = 1;
            _gamePanel.Paint += _gamePanel_Paint;
            _gamePanel.MouseDown += _gamePanel_MouseDown;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1010, 748);
            Controls.Add(_gamePanel);
            Controls.Add(_settingsPanel);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MaximizeBox = false;
            Name = "MainForm";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Tic Tac Toe";
            _settingsPanel.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private Panel _settingsPanel;
        private DrawPanel _gamePanel;
        private Label _gameStateLabel;
        private Button _startButton;
        private ListBox _zeroPlayerListBox;
        private ListBox _crossPlayerListBox;
        private Button _clearButton;
    }
}