﻿namespace UI.WinForms;

public class DrawPanel : Panel
{
    public DrawPanel()
    {
        SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer | ControlStyles.UserPaint, true);
    }
}