﻿using Common.Business;
using Common.Models;
using Common.Models.Enums;

namespace UI.WinForms;

public partial class MainForm : Form
{
    private readonly IGameService _gameService;
    private readonly IPlayerService _playerService;

    private Game _game;


    private bool _isStarted = true;
    private readonly Pen _penFigure = new(Color.Green, 10);

    private readonly Pen _penLine = new(Color.Black, 10);

    public MainForm() : this(null, null)
    {
    }

    public MainForm(IGameService gameService, IPlayerService playerService)
    {
        _gameService = gameService;
        _playerService = playerService;
        InitializeComponent();

        Application.Idle += Application_Idle;
        Application.ApplicationExit += Application_ApplicationExit;

        _crossPlayerListBox.Items.Clear();
        _zeroPlayerListBox.Items.Clear();
    }

    private void Application_ApplicationExit(object? sender, EventArgs e)
    {
        Application.Idle -= Application_Idle;
    }


    private void _crossPlayerListBox_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    private void _zeroPlayerListBox_SelectedIndexChanged(object sender, EventArgs e)
    {
    }

    private void _startButton_Click(object sender, EventArgs e)
    {
        _isStarted = true;
        var crossPlayer = (Player)_crossPlayerListBox.SelectedItems[0];
        var zeroPlayer = (Player)_zeroPlayerListBox.SelectedItems[0];
        _game = _gameService.CreateGame(crossPlayer.Id, zeroPlayer.Id);
    }

    private void _clearButton_Click(object sender, EventArgs e)
    {
        _isStarted = false;
        _game = null;
        var players = _playerService.GetList();

        _crossPlayerListBox.DataSource = new List<Player>(players);
        _crossPlayerListBox.SelectedIndex = 0;
        _crossPlayerListBox.ValueMember = nameof(Player.Id);
        _crossPlayerListBox.DisplayMember = nameof(Player.Name);

        _zeroPlayerListBox.DataSource = new List<Player>(players);
        _zeroPlayerListBox.SelectedIndex = 0;
        _zeroPlayerListBox.ValueMember = nameof(Player.Id);
        _zeroPlayerListBox.DisplayMember = nameof(Player.Name);
    }

    private void _gamePanel_MouseDown(object sender, MouseEventArgs e)
    {
        if (_game == null) return;

        if (_game.State != GameState.CrossMove && _game.State != GameState.ZeroMove) return;
        var width = _gamePanel.Width / 3;
        var height = _gamePanel.Height / 3;
        var X = e.X / width;
        var Y = e.Y / height;
        if (_game.Moves.FirstOrDefault(m => m.X == X && m.Y == Y) != null) return;

        _game = _gameService.MakeNextMove(_game.Id, X, Y);
    }

    private void Application_Idle(object? sender, EventArgs e)
    {
        _startButton.Enabled = !_isStarted;
        _crossPlayerListBox.Enabled = !_isStarted;
        _zeroPlayerListBox.Enabled = !_isStarted;

        if (_game == null)
            _gameStateLabel.Text = "";
        else
            switch (_game.State)
            {
                case GameState.Draw:
                    _gameStateLabel.Text = "Draw";
                    break;
                case GameState.CrossMove:
                    _gameStateLabel.Text = "Cross Move";
                    break;
                case GameState.ZeroMove:
                    _gameStateLabel.Text = "Zero Move";
                    break;
                case GameState.CrossWin:
                    _gameStateLabel.Text = "Cross Win";
                    break;
                case GameState.ZeroWin:
                    _gameStateLabel.Text = "Zero Win";
                    break;
            }


        _gamePanel.Invalidate();
    }

    private void _gamePanel_Paint(object sender, PaintEventArgs e)
    {
        const int offset = 30;
        var g = e.Graphics;
        var width = _gamePanel.Width / 3;
        var height = _gamePanel.Height / 3;
        g.DrawLine(_penLine, width, 0, width, height * 3);
        g.DrawLine(_penLine, width * 2, 0, width * 2, height * 3);
        g.DrawLine(_penLine, 0, height, width * 3, height);
        g.DrawLine(_penLine, 0, height * 2, width * 3, height * 2);

        if (_game == null) return;

        foreach (var move in _game.Moves)
            if (move.Number % 2 == 0)
            {
                g.DrawLine(_penFigure, move.X * width + offset, move.Y * height + offset, (move.X + 1) * width - offset,
                    (move.Y + 1) * height - offset);
                g.DrawLine(_penFigure, (move.X + 1) * width - offset, move.Y * height + offset, move.X * width + offset,
                    (move.Y + 1) * height - offset);
            }
            else
            {
                g.DrawEllipse(_penFigure, move.X * width + offset, move.Y * height + offset, width - offset * 2,
                    height - offset * 2);
            }
    }
}