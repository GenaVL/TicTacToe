using Business;
using Common.Business;
using Common.DataAccess;
using DataAccess.Xml.Persistence;
using DataAccess.Xml.Repositories;
using DataAccess.Xml;
using Infrastructure.Peaa;
using Infrastructure;
using UI.BlazorApp.Components;
using WebApi.Proxy;
using DataAccess.Json.Persistence;
using DataAccess.Json.Repositories;
using DataAccess.Json;
using DataAccess.Ef.Sql;
using DataAccess.Ef.Sql.Repositories;
using UI.BlazorApp.Di;

namespace UI.BlazorApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            IServiceCollection serviceCollection = builder.Services;

            serviceCollection
                .AddRazorComponents()
                .AddInteractiveServerComponents();

            IConfigurationSection diConfiguration = builder.Configuration.GetSection("Di");
            string diValue = diConfiguration.Value;

            Configuration.ConfigureServices(serviceCollection, diValue);

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseAntiforgery();

            app.MapRazorComponents<App>()
                .AddInteractiveServerRenderMode();

            app.Run();
        }
    }
}
