﻿using Common.Business;
using Common.Models;
using Common.Models.Enums;
using Microsoft.AspNetCore.Components;

namespace UI.BlazorApp.Components.Pages
{
    public partial class GamePage
    {
        private List<Player> _players = null;
        private Game _game = null;

        private Guid? _crossId = default;
        private Guid? _zeroId = default;
        private bool _isStarted = true;

        protected override void OnInitialized()
        {
            IPlayerService playerService = PlayerService;
            IGameService gameService = GameService;
            base.OnInitialized();
        }

        public void CellClick(int x, int y)
        {
            if (_game == null)
            {
                return;
            }

            if (_game.State != GameState.CrossMove && _game.State != GameState.ZeroMove)
            {
                return;
            }

            if (_game.Moves.FirstOrDefault(m => m.X == x && m.Y == y) != null)
            {
                return;
            }

            _game = GameService.MakeNextMove(_game.Id, x, y);

        }

        public void StartGameHandler()
        {
            _isStarted = true;
            _game = GameService.CreateGame(_crossId.Value, _zeroId.Value);
            StateHasChanged();
        }

        public void ClearGameHandler()
        {
            _players = PlayerService.GetList();
            _game = null;
            _isStarted = false;
            StateHasChanged();
        }

        public void CrossListClickHandler(ChangeEventArgs args)
        {
            _crossId = Guid.Parse((string)args.Value);
            StateHasChanged();
        }

        public void ZeroListClickHandler(ChangeEventArgs args)
        {
            _zeroId = Guid.Parse((string)args.Value);
            StateHasChanged();
        }

        public Tuple<string, string, string> CalcRow(int row)
        {
            if (_game == null)
            {
                return new Tuple<string, string, string>("", "", "");
            }

            string s0 = CalcCell(row, 0);
            string s1 = CalcCell(row, 1);
            string s2 = CalcCell(row, 2);
            return new Tuple<string, string, string>(s0, s1, s2);
        }

        private string CalcCell(int row, int col)
        {
            string cell = "";
            Move move = _game.Moves.FirstOrDefault(m => m.X == col && m.Y == row);
            if (move != null)
            {
                cell = move.Number % 2 == 0 ? "X" : "0";
            }
            return cell;
        }

        public bool IsDisabled => _isStarted;
        public bool IsDisabledStartButton => _isStarted || _crossId == null || _zeroId == null;

        public string GameStateText
        {
            get
            {
                if (_game == null)
                {
                    return "";
                }
                switch (_game.State)
                {
                    case GameState.Draw: return "Draw";
                    case GameState.CrossMove: return "Cross Move";
                    case GameState.ZeroMove:return "Zero Move";
                    case GameState.CrossWin: return "Cross Win";
                    case GameState.ZeroWin: return "Zero Win";
                }
                return "";
            }
        } 
    }
}
