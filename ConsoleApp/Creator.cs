﻿using Common.Models;
using Common.Models.Enums;

namespace ConsoleApp;

public class Creator
{
    private const int PlayersAmount = 8;

    public Tuple<List<Player>, List<Game>> Run()
    {
        var players = CreatePlayers();

        var games = new List<Game>();

        games.Add(GreateGame1(players));
        games.Add(GreateGame2(players));

        return new Tuple<List<Player>, List<Game>>(players, games);
    }

    private static List<Player> CreatePlayers()
    {
        var players = new List<Player>();
        for (var i = 0; i < PlayersAmount; i++)
            players.Add(new Player
            {
                Id = Guid.NewGuid(),
                Name = new string((char)('A' + i), 8)
            });

        return players;
    }

    private static Game GreateGame1(List<Player> players)
    {
        var game = new Game
        {
            CrossId = players[0].Id,
            ZeroId = players[1].Id,
            State = GameState.CrossWin
        };
        game.Moves.Add(new Move { Number = 0, X = 0, Y = 0 });
        game.Moves.Add(new Move { Number = 1, X = 1, Y = 0 });
        game.Moves.Add(new Move { Number = 2, X = 1, Y = 1 });
        game.Moves.Add(new Move { Number = 3, X = 2, Y = 0 });
        game.Moves.Add(new Move { Number = 4, X = 2, Y = 2 });
        return game;
    }

    private static Game GreateGame2(List<Player> players)
    {
        var game = new Game
        {
            CrossId = players[1].Id,
            ZeroId = players[0].Id,
            State = GameState.Draw
        };
        game.Moves.Add(new Move { Number = 0, X = 0, Y = 0 });
        game.Moves.Add(new Move { Number = 1, X = 1, Y = 1 });
        game.Moves.Add(new Move { Number = 2, X = 2, Y = 0 });
        game.Moves.Add(new Move { Number = 3, X = 2, Y = 2 });
        game.Moves.Add(new Move { Number = 4, X = 2, Y = 1 });
        game.Moves.Add(new Move { Number = 5, X = 1, Y = 0 });
        game.Moves.Add(new Move { Number = 6, X = 0, Y = 2 });
        game.Moves.Add(new Move { Number = 7, X = 0, Y = 1 });
        game.Moves.Add(new Move { Number = 8, X = 1, Y = 2 });
        return game;
    }
}