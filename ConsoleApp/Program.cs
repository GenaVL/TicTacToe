﻿using DataAccess.Json.Persistence;

namespace ConsoleApp;

internal class Program
{
    private static void Main(string[] args)
    {
        //Creator creator = new Creator();
        //Tuple<List<Player>, List<Game>> tuple = creator.Run();
        //Saver saver = new Saver();
        //saver.Run(tuple);

        //XmlFileStorage storage = new XmlFileStorage(@"c:\Games\TicTacToe.game");
        //XDocument doc = storage.Load();
        //XmlSerializer serializer = new XmlSerializer();
        //Context context = serializer.Deserialize(doc);
        //int n = 0;


        //Creator creator = new Creator();
        //Tuple<List<Player>, List<Game>> tuple = creator.Run();
        //SaverJson saver = new SaverJson();
        //saver.Run(tuple);

        var storage = new JsonFileStorage(@"c:\Games\TicTacToe.json");
        var json = storage.Load();
        var serializer = new JsonSerializer();
        var context = serializer.Deserialize(json);
        var n = 0;
    }
}