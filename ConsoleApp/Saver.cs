﻿using Common.Models;
using DataAccess.Xml;
using DataAccess.Xml.Persistence;

namespace ConsoleApp;

public class Saver
{
    private const string filePath = @"C:\\Games\xxxxx.txt";

    public void Run(Tuple<List<Player>, List<Game>> tuple)
    {
        var context = new XmlContext();
        context.Players.AddRange(tuple.Item1);
        context.Games.AddRange(tuple.Item2);
        var serializer = new XmlSerializer();
        var doc = serializer.Serialize(context);
        var storage = new XmlFileStorage(filePath);
        storage.Save(doc);
    }
}