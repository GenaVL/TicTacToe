﻿using Common.Models;
using DataAccess.Json;
using DataAccess.Json.Persistence;

namespace ConsoleApp;

public class SaverJson
{
    private const string filePath = @"C:\\Games\xxxxx.txt";

    public void Run(Tuple<List<Player>, List<Game>> tuple)
    {
        var context = new JsonContext();
        context.Players.AddRange(tuple.Item1);
        context.Games.AddRange(tuple.Item2);
        var serializer = new JsonSerializer();
        var json = serializer.Serialize(context);
        var storage = new JsonFileStorage(filePath);
        storage.Save(json);
    }
}