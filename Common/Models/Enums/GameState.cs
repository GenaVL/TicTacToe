﻿namespace Common.Models.Enums;

public enum GameState
{
    CrossMove = 0,
    ZeroMove = 1,
    Draw = 2,
    CrossWin = 3,
    ZeroWin = 4
}