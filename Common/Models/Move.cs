﻿namespace Common.Models;

public class Move : Entity
{
    public int Number { get; set; }
    public int X { get; set; }
    public int Y { get; set; }
}