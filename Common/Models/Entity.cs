﻿namespace Common.Models;

public abstract class Entity
{
    protected Entity()
    {
        Id = Guid.NewGuid();
    }

    public Guid Id { get; set; }

    public override string ToString()
    {
        return $"{GetType().Name} {Id:N}";
    }
}