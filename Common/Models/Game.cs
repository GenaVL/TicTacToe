﻿using Common.Models.Enums;

namespace Common.Models;

public class Game : Entity
{
    public Player Cross { get; set; }
    public Guid CrossId { get; set; }
    public Player Zero { get; set; }
    public Guid ZeroId { get; set; }

    public GameState State { get; set; }

    //TODO: Setter is workaround to avoid mapping to DTO to use in WebApi. It is acceptable only for POC!!!!
    public List<Move> Moves { get; set; } = new();
}