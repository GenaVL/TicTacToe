﻿namespace Common.Dtos;

public class PlayerStats
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public int Won { get; set; }

    public int Lost { get; set; }

    public int Draw { get; set; }
}