﻿using Common.Models;

namespace Common.Business;

public interface IGameService
{
    Game CreateGame(Guid crossId, Guid zeroId);
    Game MakeNextMove(Guid gameId, int x, int y);
    Game GetGame(Guid gameId);
}