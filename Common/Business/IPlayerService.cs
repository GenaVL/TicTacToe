﻿using Common.Models;

namespace Common.Business;

public interface IPlayerService
{
    List<Player> GetList();
}