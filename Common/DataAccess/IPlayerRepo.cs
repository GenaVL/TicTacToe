﻿using Common.Models;

namespace Common.DataAccess;

public interface IPlayerRepo
{
    Player GetById(Guid id);

    List<Player> GetList();
}