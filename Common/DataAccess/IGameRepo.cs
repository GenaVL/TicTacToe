﻿using Common.Models;

namespace Common.DataAccess;

public interface IGameRepo
{
    Game GetById(Guid id);
    void AddGame(Game game);
    void UpdateGame(Game game);
}