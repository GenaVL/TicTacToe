﻿using Common.Dtos;

namespace Common.Queries
{
    public interface IPlayerStatsQueries
    {
        List<PlayerStats> GetList();

        PlayerStats GetById(Guid id);
    }
}
