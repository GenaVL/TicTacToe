using Common.Business;
using Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using UI.WinForms;
using WebApi.Proxy;

namespace WinForms.WebApi.DalXml;

internal static class Program
{
    public static IServiceProvider ServiceProvider { get; set; }

    /// <summary>
    ///     The main entry point for the application.
    /// </summary>
    [STAThread]
    private static void Main()
    {
        var host = Host
            .CreateDefaultBuilder()
            .ConfigureServices(ConfigureServices)
            .Build();
        ServiceProvider = host.Services;

        ApplicationConfiguration.Initialize();
        Application.Run(ServiceProvider.GetRequiredService<MainForm>());
    }

    private static void ConfigureServices(HostBuilderContext context, IServiceCollection services)
    {
        services.AddScoped<MainForm>(sp =>
            new MainForm(sp.GetRequiredService<IGameService>(), sp.GetRequiredService<IPlayerService>()));

        services.AddScoped<ServiceFactory>(sp => sp.GetRequiredService);

        services.AddScoped<IGameService, GameProxyService>();
        services.AddScoped<IPlayerService, PlayerProxyService>();
    }
}