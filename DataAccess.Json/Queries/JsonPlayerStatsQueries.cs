﻿using Common.Queries;
using Common.Dtos;
using Common.Models;
using Common.Models.Enums;

namespace DataAccess.Json.Queries
{
    public class JsonPlayerStatsQueries : JsonBaseQueries, IPlayerStatsQueries
    {
        public List<PlayerStats> GetList()
        {
            List<PlayerStats> playerStatsList = Context.Players.Select(p => GetByPlayer(p)).ToList();
            return playerStatsList;
        }

        public PlayerStats GetById(Guid id)
        {
            Player player = Context.Players.Single(p => p.Id == id);
            return GetByPlayer(player);
        }

        private PlayerStats GetByPlayer(Player player)
        {
            List<Game> gamesByCross = Context.Games.Where(g => g.CrossId == player.Id).ToList();
            List<Game> gamesByZero = Context.Games.Where(g => g.ZeroId == player.Id).ToList();

            int won = gamesByCross.Count(g => g.State == GameState.CrossWin) +
                      gamesByZero.Count(g => g.State == GameState.ZeroWin);
            int draw = gamesByCross.Count(g => g.State == GameState.Draw) + 
                       gamesByZero.Count(g => g.State == GameState.Draw);
            int lost = gamesByCross.Count(g => g.State == GameState.ZeroWin) +
                       gamesByZero.Count(g => g.State == GameState.CrossWin);

            PlayerStats stats = new PlayerStats()
            {
                Id = player.Id,
                Name = player.Name,
                Won = won,
                Draw = draw,
                Lost = lost
            };
            return stats;
        }
    }
}
