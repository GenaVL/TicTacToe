﻿using Common.DataAccess;
using Common.Models;

namespace DataAccess.Json.Repositories;

public class JsonPlayerRepo : JsonBaseRepo, IPlayerRepo
{
    public Player GetById(Guid id)
    {
        return Context.Players.FirstOrDefault(p => p.Id == id);
    }

    public List<Player> GetList()
    {
        return Context.Players;
    }
}