﻿using Common.Models;

namespace DataAccess.Json;

public class JsonContext
{
    public List<Player> Players { get; } = new();
    public List<Game> Games { get; } = new();
}