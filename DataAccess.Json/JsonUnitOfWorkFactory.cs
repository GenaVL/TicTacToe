﻿using DataAccess.Json.Persistence;
using Infrastructure;
using Infrastructure.Peaa;

namespace DataAccess.Json;

public class JsonUnitOfWorkFactory : IUnitOfWorkFactory
{
    private readonly IJsonSerializer _serializer;
    private readonly ServiceFactory _serviceFactory;
    private readonly IJsonStorage _storage;

    public JsonUnitOfWorkFactory(ServiceFactory serviceFactory, IJsonSerializer serializer, IJsonStorage storage)
    {
        _serviceFactory = serviceFactory;
        _serializer = serializer;
        _storage = storage;
    }

    public IUnitOfWork Create()
    {
        return new JsonUnitOfWork(_serviceFactory, _serializer, _storage);
    }
}