﻿namespace DataAccess.Json.Persistence;

public interface IJsonStorage
{
    string Load();
    void Save(string json);
}