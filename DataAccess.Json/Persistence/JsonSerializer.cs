﻿using Newtonsoft.Json;

namespace DataAccess.Json.Persistence;

public class JsonSerializer : IJsonSerializer
{
    public JsonContext Deserialize(string json)
    {
        var context = JsonConvert.DeserializeObject<JsonContext>(json);

        foreach (var game in context.Games)
        {
            game.Cross = context.Players.First(p => p.Id == game.CrossId);
            game.Zero = context.Players.First(p => p.Id == game.ZeroId);
        }

        return context;
    }

    public string Serialize(JsonContext context)
    {
        foreach (var game in context.Games)
        {
            game.Cross = null;
            game.Zero = null;
        }

        var json = JsonConvert.SerializeObject(context, Formatting.Indented);
        return json;
    }
}