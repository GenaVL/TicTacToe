﻿namespace DataAccess.Json.Persistence;

public interface IJsonSerializer
{
    string Serialize(JsonContext context);
    JsonContext Deserialize(string json);
}