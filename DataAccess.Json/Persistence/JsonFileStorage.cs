﻿namespace DataAccess.Json.Persistence;

public class JsonFileStorage : IJsonStorage
{
    public JsonFileStorage(string xmlPath)
    {
        if (string.IsNullOrEmpty(xmlPath)) throw new ArgumentNullException(nameof(xmlPath));
        FilePath = xmlPath;
    }

    public string FilePath { get; }

    public string Load()
    {
        using var stream = File.Open(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read);

        using var reader = new StreamReader(stream);

        var json = reader.ReadToEnd();
        return json;
    }

    public void Save(string json)
    {
        using var stream = File.Open(FilePath, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);
        using var writer = new StreamWriter(stream);

        writer.Write(json);
    }
}