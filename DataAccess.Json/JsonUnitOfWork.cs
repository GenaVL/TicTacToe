﻿using DataAccess.Json.Persistence;
using DataAccess.Json.Repositories;
using Infrastructure;
using Infrastructure.Peaa;

namespace DataAccess.Json;

public class JsonUnitOfWork : IUnitOfWork
{
    private readonly IJsonSerializer _serializer;
    private readonly ServiceFactory _serviceFactory;
    private readonly IJsonStorage _storage;

    private JsonContext _context;

    public JsonUnitOfWork(
        ServiceFactory serviceFactory,
        IJsonSerializer serializer,
        IJsonStorage storage)
    {
        _serviceFactory = serviceFactory;
        _serializer = serializer;
        _storage = storage;
    }

    public T GetRepository<T>()
    {
        var repository = _serviceFactory.GetInstance<T>();
        if (!(repository is JsonBaseRepo repo)) throw new InvalidCastException($"{typeof(T)}");
        repo.Context = GetContext();
        return repository;
    }

    public void RejectChanges()
    {
        _context = null;
        GetContext();
    }

    public void SaveChanges()
    {
        SaveContext();
    }

    public void Dispose()
    {
    }

    private JsonContext GetContext()
    {
        if (_context == null)
        {
            var json = _storage.Load();
            _context = _serializer.Deserialize(json);
        }

        return _context;
    }

    public void SaveContext()
    {
        var json = _serializer.Serialize(_context);
        _storage.Save(json);
    }
}