﻿using Common.Business;
using Common.DataAccess;
using Common.Models;
using Common.Models.Enums;
using Infrastructure.Peaa;

namespace Business;

public class GameService : IGameService
{
    private readonly IUnitOfWorkFactory _unitOfWorkFactory;

    public GameService(IUnitOfWorkFactory unitOfWorkFactory)
    {
        _unitOfWorkFactory = unitOfWorkFactory;
    }

    public Game CreateGame(Guid crossId, Guid zeroId)
    {
        using var unitOfWork = _unitOfWorkFactory.Create();

        var playerRepo = unitOfWork.GetRepository<IPlayerRepo>();
        var crossPlayer = playerRepo.GetById(crossId);
        var zeroPlayer = playerRepo.GetById(zeroId);

        var game = new Game();
        game.CrossId = crossPlayer == null ? throw new NullReferenceException("Cross Player") : crossPlayer.Id;
        game.ZeroId = zeroPlayer == null ? throw new NullReferenceException("Zero Player") : zeroPlayer.Id;
        game.Cross = crossPlayer;
        game.Zero = zeroPlayer;
        game.State = GameState.CrossMove;

        var gameRepo = unitOfWork.GetRepository<IGameRepo>();
        gameRepo.AddGame(game);

        unitOfWork.SaveChanges();

        return game;
    }

    public Game MakeNextMove(Guid gameId, int x, int y)
    {
        if (x < 0 || y < 0 || x >= 3 || y >= 3) throw new AbandonedMutexException("Wrong coordinates");

        using var unitOfWork = _unitOfWorkFactory.Create();

        var gameRepo = unitOfWork.GetRepository<IGameRepo>();

        var game = gameRepo.GetById(gameId);

        if (gameRepo == null) throw new ArgumentException("Invalid Game");

        if (game.State == GameState.Draw || game.State == GameState.CrossWin || game.State == GameState.ZeroWin)
            throw new ArgumentException("Games over");

        if (game.Moves.FirstOrDefault(m => m.X == x && m.Y == y) != null) throw new ArgumentException("Invalid Move");

        game.Moves.Add(new Move
        {
            X = x,
            Y = y,
            Number = game.Moves.Count
        });
        game.State = CalcGameState(game);

        gameRepo.UpdateGame(game);

        unitOfWork.SaveChanges();

        return game;
    }

    public Game GetGame(Guid gameId)
    {
        using var unitOfWork = _unitOfWorkFactory.Create();

        var gameRepo = unitOfWork.GetRepository<IGameRepo>();

        var game = gameRepo.GetById(gameId);

        if (gameRepo == null) throw new ArgumentException("Invalid Game");

        return game;
    }

    private GameState CalcGameState(Game game)
    {
        if (game == null) throw new NullReferenceException("Game");

        if (game.State == GameState.Draw || game.State == GameState.CrossWin || game.State == GameState.ZeroWin)
            return game.State;

        if (FindLine(game.Moves.Where(m => m.Number % 2 == 0).ToList())) return GameState.CrossWin;
        if (FindLine(game.Moves.Where(m => m.Number % 2 == 1).ToList())) return GameState.ZeroWin;

        if (game.Moves.Count() == 9) return GameState.Draw;
        return game.Moves.Count() % 2 == 0 ? GameState.CrossMove : GameState.ZeroMove;
    }

    private bool FindLine(List<Move> moves)
    {
        return CalcHor(moves, 0)
               || CalcHor(moves, 1)
               || CalcHor(moves, 2)
               || CalcVert(moves, 0)
               || CalcVert(moves, 1)
               || CalcVert(moves, 2)
               || CalcDiag1(moves)
               || CalcDiag2(moves);
    }

    private static bool CalcHor(List<Move> moves, int index)
    {
        return moves.Count(m => m.X == index) == 3;
    }

    private static bool CalcVert(List<Move> moves, int index)
    {
        return moves.Count(m => m.Y == index) == 3;
    }

    private static bool CalcDiag1(List<Move> moves)
    {
        return moves.Count(m => m.X == m.Y) == 3;
    }

    private static bool CalcDiag2(List<Move> moves)
    {
        return moves.Count(m => m.X == 2 - m.Y) == 3;
    }
}