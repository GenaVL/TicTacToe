﻿using Common.Business;
using Common.DataAccess;
using Common.Models;
using Infrastructure.Peaa;

namespace Business;

public class PlayerService : IPlayerService
{
    private readonly IUnitOfWorkFactory _unitOfWorkFactory;

    public PlayerService(IUnitOfWorkFactory unitOfWorkFactory)
    {
        _unitOfWorkFactory = unitOfWorkFactory;
    }

    public List<Player> GetList()
    {
        using var unitOfWork = _unitOfWorkFactory.Create();

        var playerRepo = unitOfWork.GetRepository<IPlayerRepo>();
        List<Player> players = playerRepo.GetList();

        return players;
    }
}