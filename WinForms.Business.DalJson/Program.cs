using System.Configuration;
using Business;
using Common.Business;
using Common.DataAccess;
using DataAccess.Json;
using DataAccess.Json.Persistence;
using DataAccess.Json.Repositories;
using Infrastructure;
using Infrastructure.Peaa;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using UI.WinForms;

namespace WinForms.Business.DalJson;

internal static class Program
{
    public static IServiceProvider ServiceProvider { get; set; }

    /// <summary>
    ///     The main entry point for the application.
    /// </summary>
    [STAThread]
    private static void Main()
    {
        var host = Host
            .CreateDefaultBuilder()
            .ConfigureServices(ConfigureServices)
            .Build();
        ServiceProvider = host.Services;

        ApplicationConfiguration.Initialize();
        Application.Run(ServiceProvider.GetRequiredService<MainForm>());
    }

    private static void ConfigureServices(HostBuilderContext context, IServiceCollection services)
    {
        var filePath = ConfigurationManager.AppSettings["FilePath"];

        services.AddScoped<MainForm>(sp =>
            new MainForm(sp.GetRequiredService<IGameService>(), sp.GetRequiredService<IPlayerService>()));

        services.AddScoped<ServiceFactory>(sp => sp.GetRequiredService);

        services.AddScoped<IGameService, GameService>();
        services.AddScoped<IPlayerService, PlayerService>();

        services.AddScoped<IUnitOfWorkFactory, JsonUnitOfWorkFactory>();

        services.AddScoped<IJsonSerializer, JsonSerializer>();
        services.AddScoped<IJsonStorage>(sp => new JsonFileStorage(filePath));

        services.AddScoped<IPlayerRepo, JsonPlayerRepo>();
        services.AddScoped<IGameRepo, JsonGameRepo>();
    }
}